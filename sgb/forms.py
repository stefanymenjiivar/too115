from django import forms
from .models import Biblioteca, Item,ItemEspecifico
from .models import Persona

#--------------------- LOOGIN--------------------------------------------------|
class LoginForm(forms.Form):
   username = forms.CharField( max_length=120)
   password = forms.CharField( max_length=120, widget= forms.PasswordInput())

#--------------------------------Biblioteca--------------------------------------|
class BibliotecaForm(forms.ModelForm):
    class Meta:
        model = Biblioteca
        fields = {'nombreBiblioteca','direccion','telefono'}

        labels = {
                    'nombreBiblioteca'      : 'Nombre',
                    'direccion'             : 'Dirección',
                    'telefono'              : 'Teléfono',
                 }
        widget = {
                    'nombreBiblioteca'      : forms.TextInput(attrs={'class':'form-control'}),
                    'direccion'             : forms.TextInput(attrs={'class':'form-control'}),
                    'telefono'              : forms.TextInput(attrs={'class':'form-control'}),
                  }


# ---------------------------------------- Item ---------------------------------------------

class itemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = [
        'idItem',
        'idSubCategoria',
        'idAutor',
        'nombreItem',
        'descrpcionItem',
        'imagenUrl',
        'fechaPublicacion'
        ]
        labels = {
        'idItem': 'ID',
        'idSubCategoria': 'SubCategoria',
        'idAutor': 'Autor',
        'nombreItem': 'Nombre',
        'descrpcionItem': 'Descripción',
        'fechaPublicacion': 'Fecha',
        }
        widgets = {
        'idItem': forms.TextInput(attrs={'class':'form-control', 'id':'id_item', 'placeholder': 'Categoria Subcategoria Serie, Ejemplo: 0200001'}),
        'idSubCategoria': forms.Select(attrs={'class': 'form-control', 'id': 'id_subcategoria'}),
        'idAutor': forms.Select(attrs={'class':'form-control', 'id': 'id_autor'}),
        'nombreItem': forms.TextInput(attrs={'class':'form-control', 'id': 'id_nombre'}),
        'descrpcionItem': forms.Textarea(attrs={'class':'form-control', 'id': 'descripcion'}),
        'fechaPublicacion': forms.DateTimeInput(attrs={'class':'form-control', 'id':'fecha', 'placeholder': 'dd/mm/aaaa'}),
        }

class itemFormEditar(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(itemFormEditar, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['idItem'].widget.attrs['readonly'] = True
            self.fields['fechaPublicacion'].required = False

    def clean_idItem(self):
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            return instance.idItem
        else:
            return self.cleaned_data['idItem']
    class Meta:
        model = Item
        fields = [
        'idItem',
        'idSubCategoria',
        'idAutor',
        'nombreItem',
        'descrpcionItem',
        'imagenUrl',
        'fechaPublicacion'
        ]
        labels = {
        'idItem': 'ID',
        'idSubCategoria': 'SubCategoria',
        'idAutor': 'Autor',
        'nombreItem': 'Nombre',
        'descrpcionItem': 'Descripción',
        'imagenUrl': 'Imagen',
        'fechaPublicacion': 'Fecha',
        }
        widgets = {
        'idItem': forms.TextInput(attrs={'class':'form-control', 'id':'id_item'} ),
        'idSubCategoria': forms.Select(attrs={'class': 'form-control', 'id': 'id_subcategoria'}),
        'idAutor': forms.Select(attrs={'class':'form-control', 'id': 'id_autor'}),
        'nombreItem': forms.TextInput(attrs={'class':'form-control', 'id': 'id_nombre'}),
        'descrpcionItem': forms.Textarea(attrs={'class':'form-control', 'id': 'descripcion'}),
        'fechaPublicacion': forms.DateTimeInput(attrs={'class':'form-control', 'id':'fecha'}),
        }

# ---------------------------------------- Item específico ------------------------------------

class itemEspecificoForm(forms.ModelForm):
    class Meta:
        model = ItemEspecifico
        fields = ['idItem', 'idBiblioteca', 'volumen', 'editorial',
        ]
        labels = {'idItem': 'Item', 'idBiblioteca': 'Biblioteca', 'volumen': 'Volumen',
        'editorial':'Editorial',
        }
        widgets = {
        'idItem': forms.Select(attrs={'class':'form-control', 'id':'id_item'} ),
        'idBiblioteca': forms.Select(attrs={'class': 'form-control', 'id': 'id_bib'}),
        'volumen': forms.TextInput(attrs={'class':'form-control', 'id': 'vol'}),
        'editorial': forms.TextInput(attrs={'class':'form-control', 'id': 'editorial'}),
        }
